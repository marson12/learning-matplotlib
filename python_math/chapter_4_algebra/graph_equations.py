from sympy import Symbol, pprint, simplify, init_printing, solve
from sympy.plotting import plot

x = Symbol('x')

func1 = 4*x+1
func2 = -x/4+1
p=plot(func1, func2, xlim=[-10,10], ylim=[-10,10],legend=True, show=False)
p[0].line_color = 'b'
p[1].line_color = 'r'
p.show()