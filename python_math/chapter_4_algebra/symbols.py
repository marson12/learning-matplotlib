from sympy import Symbol, pprint, simplify, init_printing, solve

def print_seq(num):
    if num < 1:
        pass
    init_printing(order='rev-lex')

    x = Symbol('x')
    func = x
    for count in range(2,num+1):
        current = x**count
        current /= count
        func =func + current

    pprint(func)


def print_calc_seq(seq, value):
    if seq < 1:
        pass
    init_printing(order='rev-lex')

    x = Symbol('x')
    func = x
    for count in range(2, seq + 1):
        current = x**count
        current /= count
        func = func + current

    pprint(func)
    print(func.subs({x: value}))


def show_quadratic_equation():
    x = Symbol('x')
    a = Symbol('a')
    b = Symbol('b')
    c = Symbol('c')
    func = a * x * x + b * x + c
    solved = solve(func, x, dict=True)
    pprint(solved)


x = Symbol('x')
y = Symbol('y')

exp1 = 2*x+3*y-6
exp2 = 3*x+2*y-12
answer = solve((exp1, exp2))
pprint((answer))