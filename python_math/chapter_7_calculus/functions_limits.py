from sympy import sin, solve, Symbol, Limit, S, pprint
from sympy.plotting import plot

def symbols_reminder():
    u = Symbol('u')
    t = Symbol('t')
    g = Symbol('g')
    theta = Symbol('theta')
    solved = solve(u*sin(theta)-g*t, t)
    print(solved)


def symbols_assumptions():
    x = Symbol('x', positive=True)
    if x+5 > -4:
        print('staying positive')
    else:
        print('im feeling negative today')


def print_asymptote():
    x = Symbol('x')
    fx = 1/x
    p = plot(fx, xlim=[-10, 10], ylim=[-10, 10], legend=True, show=False)
    p.show()


def limit():
    x = Symbol('x')

    func = 1/x
    pprint(func)

    lim = Limit(func, x, S.Infinity)
    print('as x approaches infinity, y approaches:', lim.doit())

    lim2 = Limit(func, x, 0)
    print('as x approaches 0 from the right, y approaches:', lim2.doit())

    # to 0 from the left
    lim3 = Limit(func, x, 0, dir='-')
    print('as x approaches 0 from the left, y approaches:', lim3.doit())

    func2 = (x**2+2*x+1)/(x+1)
    print(func2)

    lim4 = Limit(func2, x, -1)
    print('as x approaches -1, y approaches:', lim4.doit())

    #   make e
    func3 = (1+1/x)**x
    lim5 = Limit(func3, x, S.Infinity)
    print('make the number e', lim5.doit())


def compound_interest():
    p = Symbol('p')
    t = Symbol('t')
    r = Symbol('r')
    n = Symbol('n')
    # func = p*(1+r/n)**(n*t)
    func = 500*(1+.07/n)**(n*45)
    lim = Limit(func, n, S.Infinity)
    print(lim.doit())


def rate_of_change(value=None):
    t = Symbol('t')
    # 2x^2+2x+8
    st = 5*t**2+2*t+8
    t1 = Symbol('t1')
    delta_t = Symbol('delta_t')
    st1 = st.subs({t: t1})
    st1_delta = st.subs({t: t1 + delta_t})
    lim = Limit((st1_delta-st1)/delta_t, delta_t, 0)
    print('d/dx =', lim.doit())
    if value != None:
        print('value of d/dx at', value, 'is:', lim.doit().subs({t1: value}))


rate_of_change(5)