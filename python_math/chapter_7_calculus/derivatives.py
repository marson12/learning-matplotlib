from sympy import Symbol, Derivative, solve, pprint

t = Symbol('t')
x = Symbol('x')


def first_deriv():
    func = 5 * t ** 2 + 2 * t + 8
    der_func = Derivative(func, t).doit()
    print(der_func)
    # at point
    point = der_func.subs({t: 4})
    print(point)


def derivative_calculator(func, variable):
    der_func = Derivative(func, variable).doit()
    return der_func


def derivative_at_point(func, variable, x_value):
    derFunc = derivative_calculator(func, variable)
    return derFunc.subs({variable: x_value})




function = (x + 1) * (x + 2) * (x + 3) * (x + 4) * (x + 5)-100
# function = x ** 3 - x ** 2 + x - 1
# function = x**4 -10
pprint(function)
print(derivative_calculator(function, x))
print(derivative_at_point(function, x, 5))
