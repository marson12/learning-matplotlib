from sympy import Integral, Symbol

x = Symbol('x')
k = Symbol('k')


def indefinite_integral(func, var):
    return Integral(func, var).doit()


def definite_integral(func, var, lower_bound, higher_bound):
    return Integral(func, (var, lower_bound, higher_bound)).doit()


function = x**2/3+1
print(indefinite_integral(function, x))
print(definite_integral(function, x, 0, 2))

