def inch_to_centi(inch):
    return inch * 2.54


# converts from centimeter to inches
def centi_to_inch(centi):
    return centi / 2.54

def quadratic_eq(a,b,c):
    negVersion = True
    negOutput = 0

    while negVersion:
        if(negOutput !=0):
            negVersion= False

        answer = (b**2 - 4*a*c)**.5
        if negVersion:
            answer = b*-1 -answer
        else:
            answer = b*-1+answer

        answer = answer/(2*a)
        if negVersion:
            negOutput = answer

        else:
            return answer, negOutput

print(quadratic_eq(1,-6,9))