from sympy import FiniteSet
import math

print([print(x) for x in [1,2,3,4,5]])



s = FiniteSet(2,4.5,math.pi)
print(s)

def pendulum_period(length, gravity):
    return 2*math.pi*math.sqrt(length/gravity)


lengths = FiniteSet(15,18,21,22.5,25)
gravities = FiniteSet(9.8, 9.78, 9.83)
combo = lengths*gravities
for elm in combo:
    print("gravity: "+ str(elm[1])+" length: "+str(elm[0])+" time: "+str(pendulum_period(elm[0]/100, elm[1])))


########
#   PROBABILITY
########

def probability(space, event):
    return len(event)/len(space)

