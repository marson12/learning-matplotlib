import math
from collections import Counter
from matplotlib.pyplot import plot, show, title, xlabel, ylabel, legend, axis, savefig


def mean(num_list):
    return sum(num_list) / len(num_list)


def median(list):
    list.sort()
    if len(list) % 2 != 0:
        middle = (len(list) + 1) / 2
        middle = int(middle) - 1
        return list[middle]

    else:
        top_middle_elm = math.ceil(len(list) / 2)
        top_val = list[top_middle_elm]
        bottom_val = list[top_middle_elm - 1]
        return mean([top_val, bottom_val])


def mode(list):
    c = Counter(list)
    numbers_freq = c.most_common()
    max_count = numbers_freq[0][1]

    nodes = []
    for num in numbers_freq:
        if num[1] == max_count:
            nodes.append(num[0])
    return nodes


def frequency_table(list):
    if len(list) == 0:
        print("Score Frequency")
        return []

    list.sort()
    freq = []

    current = list[0]
    count = 0

    print("Score Frequency")

    for num in list:
        if num == current:
            count += 1

        else:
            freq.append((current, count))
            print('{0}\t   {1}'.format(current, count))
            count = 1
            current = num
    freq.append((current, count))
    print(str(current) + "      " + str(count))
    return freq


def range(list):
    smol = min(list)
    big = max(list)
    return big - smol


def variance(nums):
    avg = mean(nums)
    sum = 0

    for num in nums:
        var = avg - num
        var = var ** 2
        sum += var

    return sum / len(nums)


def standard_dev(nums):
    var = variance(nums)
    return var ** (1 / 2)


def find_correlation(lista, listb):
    n = len(lista)

    prod = []
    for x, y in zip(lista, listb):
        prod.append(x * y)

    sum_prod_x_y = sum(prod)
    sum_x = sum(lista)
    sum_y = sum(listb)
    square_sum_x = sum_x ** 2
    square_sum_y = sum_y ** 2

    x_square = []
    for x in lista:
        x_square.append(x ** 2)

    x_square_sum = sum(x_square)

    y_square = []
    for y in listb:
        y_square.append(y ** 2)

    y_square_sum = sum(y_square)

    # EQUATION
    numerator = n * sum_prod_x_y - sum_x * sum_y
    term1 = n * x_square_sum - square_sum_x
    term2 = n * y_square_sum - square_sum_y
    denom = (term1 * term2) ** .5
    return numerator / denom


high_grades = [83, 85, 84, 96, 94, 86, 87, 97, 97, 85]
admission_scores = [85, 87, 86, 97, 96, 88, 89, 98, 98, 87]
print(find_correlation(high_grades, admission_scores))

plot(high_grades, admission_scores, 'x')
show()
