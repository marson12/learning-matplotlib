import csv
import matplotlib.pyplot as plt


def scatter_plot(x,y):
    plt.scatter(x,y)
    plt.xlabel('number')
    plt.ylabel('square')
    plt.show()


def read_data(file):
    numbers = []
    with open(file) as f:
        for line in f:
            numbers.append(float(line))
    return numbers


def sum_data(file):
    s = 0
    with open(file) as f:
        for line in f:
            s = s + float(line)
    print(s)
    return s


def mean_data(file):
    list = read_data(file)
    print(sum(list)/len(list))
    return sum(list)/len(list)


def read_csv(csv_file):
    numbers = []
    squared = []
    with open(csv_file) as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            numbers.append(int(row[0]))
            squared.append(int(row[1]))
        return numbers, squared


def read_summer(csv_file):
    numbers = []
    squared = []
    with open(csv_file) as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            numbers.append(float(row[0]))
            squared.append(float(row[1]))
        return numbers, squared


# numbers, squared = read_csv('python_math/chapter_3_statistics/numbers.csv')
# scatter_plot(numbers, squared)
summer, swimming = read_summer('D:\programming\programs\python programs\python_math\chapter_three\correlate-summer.csv')
scatter_plot(summer, swimming)

