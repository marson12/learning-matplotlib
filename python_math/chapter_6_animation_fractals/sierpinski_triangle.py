import matplotlib.pyplot as plt
import random


def transform_1(p):
    x = p[0]
    y = p[1]
    return .5*x, .5*y


def transform_2(p):
    x = p[0]
    y = p[1]
    return .5*x + .5, .5*y + .5


def transform_3(p):
    x = p[0]
    y = p[1]
    return .5*x + 1, .5*y


def transform(p):
    transformations = [transform_1, transform_2, transform_3]
    t = random.choice(transformations)
    x, y = t(p)
    return x, y


def build_trajectory(p, n):
    x = [p[0]]
    y = [p[1]]
    for i in range(n):
        p = transform(p)
        x.append(p[0])
        y.append(p[1])

    return x, y


p = (1,1)
n  = 150000
x,y = build_trajectory(p,n)
plt.plot(x,y, '.')
plt.show()