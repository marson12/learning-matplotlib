import matplotlib.pyplot as plt
import random

.85
def transform_1(p):
    x = p[0]
    y = p[1]
    return .85*x+.04*y, -.04*x+.85*y+1.6

.07
def transform_2(p):
    x = p[0]
    y = p[1]
    return .2*x-.26*y, .23*x+.22*y+1.6


.07
def transform_3(p):
    x = p[0]
    y = p[1]
    return -.15*x+.28*y, .26*x+.24*y+.44

.01
def transform_4(p):
    y = p[1]
    return 0, .16*y

def transform(p):
    choice = random.random()
    if choice <=.85:
        return transform_1(p)
    elif choice <=.92:
        return transform_2(p)
    elif choice <=.99:
        return transform_3(p)
    else:
        return transform_4(p)


def draw_plant(p, n):
    x = [p[0]]
    y = [p[1]]
    for i in range(n):
        p = transform(p)
        x.append(p[0])
        y.append(p[1])

    return x, y


p = (1, 1)
n = 10000
x_vals, y_vals = draw_plant(p, n)
plt.plot(x_vals, y_vals, '.')
plt.show()