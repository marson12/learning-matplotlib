import matplotlib.pyplot as plt
from matplotlib import animation


def show_shape(patch):
    ax = plt.gca()
    ax.add_patch(patch)
    plt.axis('scaled')
    plt.show()


def create_circle():
    circle = plt.Circle((0,0), radius=0, fc='blue', ec='yellow')
    return circle

# c = create_circle()
# show_shape(c)

#
#   growing circle
#

def update_radius(i, circle, frame_count):
    print('i', i)
    print(circle.radius)

    if i < frame_count/2:
        circle.radius += .1
    else:
        circle.radius -= .1

    return circle



def create_animation():
    fig = plt.gcf()
    ax = plt.axes(xlim=(-10, 10), ylim=(-10, 10))
    # ax=plt.axis('scaled')

    ax.set_aspect('equal')
    circle = create_circle()
    ax.add_patch(circle)
    frame_count = 200
    anim = animation.FuncAnimation(fig, update_radius, fargs=(circle, frame_count), frames=frame_count, interval=1)
    plt.title('first animation')
    plt.show()


create_animation()
