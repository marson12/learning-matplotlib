import matplotlib.pyplot as plt
import matplotlib.cm as cm
x0, x1 = -2.5, 1
y0, y1 = -1, 1


def initialize_image():
    size = 1000
    percent = size/100
    max_iteration = 400

    image = []
    dx = (x1 - x0) / (size - 1)
    dy = (y1 - y0) / (size - 1)

    for y in range(size):
        x_colors = []
        for x in range(size):
            c = complex(x*dx+x0, y*dy+y0)
            z = 0
            iteration = 0
            while abs(z) < 2 and iteration < max_iteration:
                z = z*z+c
                iteration += 1

            x_colors.append(iteration)

        if y%percent == 0:
            print(int(y/size*100), '% :', y, '/', size)
        image.append(x_colors)

    print('100% :', size, '/', size)

    return image


def color_points():
    image = initialize_image()
    plt.imshow(image, origin='lower', extent=(x0, x1, y0, y1), cmap=cm.Greys_r, interpolation='nearest')
    plt.colorbar()
    plt.savefig('python_math/chapter_6_animation_fractals/fractal.pdf', )

    plt.show()



color_points()

# asdf
# asdfasdf
#
# project to make it loop through a variety of different sizes, and max_iterations,
# then time them all. save the images with the name scheme 'size.'size' iter.'max_iterations' time:'timetaken
# also figure out how to different colors in the fractals and then show lief
#  1000, 50 looks really cool. just saying