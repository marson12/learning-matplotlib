import matplotlib.pyplot as plt
import random
import math


def transform_1(p):
    x = p[0]
    y = p[1]
    return math.cos(x)+1, math.sin(y)


def transform_2(p):
    x = p[0]
    y = p[1]
    return math.sin(x), math.cos(y)


def transform_3(p):
    x = p[0]
    y = p[1]
    return x+1, y+1

def transform(p):
    transformations = [transform_1, transform_2, transform_3]
    t = random.choice(transformations)
    x, y = t(p)
    return x, y


def build_trajectory(p, n):
    x = [p[0]]
    y = [p[1]]
    for i in range(n):
        p = transform(p)
        x.append(p[0])
        y.append(p[1])

    return x, y


start = (1,1)
count = 15000
x,y = build_trajectory(start, count)
plt.plot(x, y, '.')
plt.show()