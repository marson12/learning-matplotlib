from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib.patches import Ellipse
import math
import numpy.random as rnd

g = 9.8


class Projectile:

    def __init__(self, u, theta):
        self.u = u
        self.theta = theta
        self.intervals = self.get_intervals()
        self.x_min = 0
        self.x_max = u*math.cos(theta)*self.intervals[-1]
        self.y_min = 0
        t_max = u*math.sin(theta)/g
        self.y_max = u*math.sin(theta)*t_max-.5*g*t_max**2
        self.ellipse = None
        self.active = True
        self.color = 0
        self.t_flight = 2*self.u*math.sin(self.theta)/g

    def get_intervals(self):
        self.t_flight = 2*self.u*math.sin(self.theta)/g
        intervals = []
        start = 0
        # interval = .05
        interval = self.t_flight/200
        while start < self.t_flight:
            intervals.append(start)
            start = start+interval

        print(len(intervals))
        return intervals

    def set_shape(self, y_max, y_min):
        self.color = rnd.rand(3)
        self.ellipse = Ellipse((0, 0), width=y_max / 10, height=y_min / 7.5, fc=self.color)


def update_position(i, max_intervals, projectiles):
    t = max_intervals[i]

    for proj in projectiles:
        if proj.active:
            x = proj.u * math.cos(proj.theta) * t
            y = proj.u * math.sin(proj.theta) * t - .5 * g * t * t
            if y < 0:
                proj.active = False
            else:
                proj.ellipse.center = x, y

def create_animation(projectile_specifications):

    projectiles = []
    highest_x_max = 0
    highest_y_max = 0
    highest_t_flight = 0
    max_intervals = []
    for (u,theta) in projectile_specifications:
        projectile = Projectile(u, theta)
        projectiles.append(projectile)
        if projectile.x_max > highest_x_max:
            highest_x_max = projectile.x_max

        if projectile.y_max > highest_y_max:
            highest_y_max = projectile.y_max

        if projectile.t_flight > highest_t_flight:
            max_intervals = projectile.intervals

    fig = plt.gcf()
    ax = plt.axes(xlim=(0, highest_x_max), ylim=(0, highest_y_max))

    legend_strings = []
    for proj in projectiles:
        proj.set_shape(highest_x_max, highest_y_max)
        ax.add_patch(proj.ellipse)
        legend_strings.append('u: ' + str(proj.u) + ' theta: ' + str(proj.theta))


    anim = animation.FuncAnimation(fig, update_position, fargs=(max_intervals, projectiles),
                                   frames=len(max_intervals), interval=1, repeat=False)

    ax.legend(legend_strings, loc='upper right')
    plt.title('projectile motion')
    plt.xlabel('distance')
    plt.ylabel('height')

    plt.show()


def projectiles():
    projectile_specifications = []
    projectile_specifications.append([5, math.radians(15)])
    projectile_specifications.append([5, math.radians(30)])
    projectile_specifications.append([5, math.radians(45)])
    projectile_specifications.append([5, math.radians(60)])
    projectile_specifications.append([5, math.radians(85)])
    projectile_specifications.append([5, math.radians(90)])
    return projectile_specifications


projectile_specifications = projectiles()
create_animation(projectile_specifications)









