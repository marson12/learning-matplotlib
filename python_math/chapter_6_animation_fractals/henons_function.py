import matplotlib.pyplot as plt
import random


def henon_transform(p):
    x = p[0]
    y = p[1]
    return y+1 - 1.4*x**2, .3*x


def build_trajectory(p, n):
    x = [p[0]]
    y = [p[1]]
    for i in range(n):
        p = henon_transform(p)
        x.append(p[0])
        y.append(p[1])

    return x, y


p = (0, 0)
n = 20000
x, y = build_trajectory(p, n)
plt.plot(x, y, '.')
plt.show()