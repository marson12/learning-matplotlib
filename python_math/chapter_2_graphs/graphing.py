from matplotlib.pyplot import plot, show, title, xlabel, ylabel, legend, axis,savefig

temp_2006 = [12, 13, 14, 15]
temp_2007 = [13, 24, 25, 16]
temp_2008 = [14, 15, 16, 27]

range = [1,2,3,4]
plot(temp_2006)
plot(temp_2007)
plot(temp_2008)
title("made up temps over 3 years")
xlabel('years')
ylabel('tempt')
legend([2006,2007,2008])
axis(ymin=0)
axis(ymax=100)
savefig('python_math/chapter_2_graphs/testing.png',)
