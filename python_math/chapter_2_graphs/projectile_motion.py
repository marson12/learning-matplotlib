import matplotlib.pyplot as plt
import math
# constant G
G = 9.8

#draw the graph
def draw_graph(x,y):
    plt.plot(x,y)
    plt.ylabel('force in newtons')
    plt.xlabel('distance in meters')
    plt.title('gravity in force by distance')

def plot_it():
    plt.show()

def generate_f(m1 = .5, m2=1.5):

    r = range(100,1001,1)
    f=[]

    for dist in r:
        force = G*(m1*m2)/(dist**2)
        f.append(force)

    draw_graph(r,f)

def get_time(u,theta):
    return 2*u*math.sin(theta)/G

def create_graph(u,theta):
    total_fly_time = get_time(u,theta)

    interval = .001
    fly_time = 0
    times = []
    while fly_time < total_fly_time:
        times.append(fly_time)
        fly_time += interval

    y = []
    x = []
    for time in times:
        x_value = u*math.cos(theta)*time
        y_value = u*math.sin(theta)*time - .5*G*time*time
        y.append(y_value)
        x.append(x_value)

    draw_graph(x,y)

def get_touples():
    touples = []
    # legends = []
    touples.append((10,math.pi/4))

    touples.append((20,math.pi/4))
    touples.append((30,math.pi/4))
    # start = 0
    # while start < math.pi/2:
    #     touples.append((1,start))
    #     start+=math.pi/12


    # plt.legend(legends)
    return touples

def main_func(touple_list):
    for touple in touple_list:
        create_graph(touple[0], touple[1])


if __name__=='__main__':
    touple_list = get_touples()
    main_func(touple_list)
    plot_it()



