import matplotlib.pyplot as plt

#draw the graph
def draw_graph(x,y):
    plt.plot(x,y)
    plt.ylabel('force in newtons')
    plt.xlabel('distance in meters')
    plt.title('gravity in force by distance')
    plt.show()

#generate values
def generate_f(m1 = .5, m2=1.5):
    # constant G
    G = 6.674 * (10 ** -11)

    r = range(100,1001,1)
    f=[]

    for dist in r:
        force = G*(m1*m2)/(dist**2)
        f.append(force)

    draw_graph(r,f)

if __name__=='__main__':
    generate_f()






