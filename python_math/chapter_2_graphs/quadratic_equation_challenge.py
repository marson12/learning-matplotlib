import matplotlib.pyplot as plt

#draw the graph
def draw_graph(x,y):
    plt.plot(x,y)
    plt.ylabel('force in newtons')
    plt.xlabel('distance in meters')
    plt.title('gravity in force by distance')
    plt.show()

#generate values
def generate_f():
    interval = .5
    x_lower = -10
    x_max = 10
    range = []
    while x_lower < x_max:
        range.append(x_lower)
        x_lower += interval
    f=[]

    for x in range:
        y_value = x*x+2*x+1
        f.append(y_value)

    draw_graph(range,f)

if __name__=='__main__':
    generate_f()

