import matplotlib.pyplot as plt

def fib(n):
    if n == 1:
        return [1]
    if n == 2:
        return [1, 1]
    a = 1
    b = 1
    series = [a, b]
    for i in range(n):
        c = a + b
        series.append(c)
        a = b
        b = c
    return series

def check(num):
    fibs = fib(num)
    return fibs

def generate_f():
    fibs = check(100)

    a = 0
    b = 0
    ratios=[]
    for fib in fibs:
        if a==0:
            a=fib

        elif b==0:
            b = fib
            ratios.append(b/a)
        else:
            a=b
            b=fib
            ratios.append(b/a)



    plt.plot(ratios)
    plt.show()


if __name__=='__main__':
    generate_f()

